class RegressionDataSet{
public:
  RegressionDataSet() {}

  RegressionDataSet(std::string filename){
    int s = regression_dataset_sufix.size();
    if(filename.substr(filename.size()-s,s).compare(regression_dataset_sufix) != 0){
      throw Exception("Invalid extension type!");
    }
    std::ifstream dataFile(filename, std::ifstream::binary);
    if(dataFile.is_open()){
      while(true){
        if(dataFile.eof()) break;

        std::string input="";
        std::getline(dataFile, input, '.');
        int sharp = input.find("#");
        double y = convertToValue<double>(Base64::decode(input.substr(0, sharp)));

        add(RegressionBinInput(input.substr(sharp + 1, input.size() - (sharp + 1))), y);
      }
      dataFile.close();
    }
  }

  RegressionDataSet(const std::vector<std::vector<short>>& data){
    _inputs.insert(_inputs.end(), data.begin(), data.end());
  }

  RegressionDataSet(const std::vector<std::vector<short>>& data, std::vector<double>& Y){
    if(data.size() != Y.size()){
      throw Exception("Y and data have different sizes!");
    }

    for(size_t i = 0; i < data.size(); i++){
      add(data[i], Y[i]);
    }
  }

  ~RegressionDataSet(){
    _inputs.clear();
  }

  void add(const RegressionBinInput& input, const double y){
    _inputs.push_back(input);
    _Y[_inputs.size() - 1] = y;
  }

  void add(const RegressionBinInput& input){
    _inputs.push_back(input);
  }

  void add(const std::vector<short>& input){
    _inputs.push_back(RegressionBinInput(input));
  }

  void add(const RegressionBinInput& input, const std::string& y){
    add(input, std::stod(y));
  }

  void add(const std::vector<short>& input, const double y){
    add(RegressionBinInput(input), y);
  }

  void add(const std::vector<short>& input, const std::string& y){
    add(RegressionBinInput(input), std::stod(y));
  }

  const RegressionBinInput &operator[](int index) const
  {
    return _inputs[index];
  }

  const RegressionBinInput &getInput(int index) const{
    return _inputs[index];
  }

  double getY(int index) const{
    return _Y.at(index);
  }

  size_t size() const {
    return _inputs.size();
  }

  void save(std::string prefix) {
    std::string filename = prefix + regression_dataset_sufix;
    std::ofstream dataFile(filename, std::ofstream::binary);

    for (unsigned int i = 0; i < _inputs.size(); i++){
      dataFile << (i != 0 ? "." : "") + Base64::encode(convertToBytes(_Y[i])) + "#" + _inputs[i].data();
    }

    dataFile.close();
  }

private:
  std::vector<RegressionBinInput> _inputs;
  std::unordered_map<int, double> _Y;
};
