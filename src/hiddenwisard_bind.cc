#include "hiddenwisardpkg.h"

PYBIND11_MODULE(hiddenwisardpkg, m){
  m.attr("__version__") = __version__;

  //data
  py::class_<RegressionBinInput>(m, "RegressionBinInput", py::module_local())
      .def(py::init<index_size_t>())
      .def(py::init<const std::vector<short> &>())
      .def(py::init<const std::string &>())
      .def("__getitem__", &RegressionBinInput::get)
      .def("get", &RegressionBinInput::get)
      .def("set", &RegressionBinInput::set)
      .def("size", &RegressionBinInput::size)
      .def("data", &RegressionBinInput::data);

  py::class_<RegressionDataSet>(m, "RegressionDataSet", py::module_local())
      .def(py::init())
      .def(py::init<std::string>())
      .def(py::init<const std::vector<std::vector<short>> &, std::vector<double> &>())
      .def(py::init<const std::vector<std::vector<short>> &>())
      .def("add", (void (RegressionDataSet::*)(const RegressionBinInput &)) & RegressionDataSet::add)
      .def("add", (void (RegressionDataSet::*)(const std::vector<short> &)) & RegressionDataSet::add)
      .def("add", (void (RegressionDataSet::*)(const RegressionBinInput &, const double)) & RegressionDataSet::add)
      .def("add", (void (RegressionDataSet::*)(const RegressionBinInput &, const std::string &)) & RegressionDataSet::add)
      .def("add", (void (RegressionDataSet::*)(const std::vector<short> &, const double)) & RegressionDataSet::add)
      .def("add", (void (RegressionDataSet::*)(const std::vector<short> &, const std::string &)) & RegressionDataSet::add)
      .def("getInput", &RegressionDataSet::getInput)
      .def("getY", &RegressionDataSet::getY)
      .def("size", &RegressionDataSet::size)
      .def("save", &RegressionDataSet::save);

  // binarizations
  py::class_<KernelThermometer>(m, "KernelThermometer", py::module_local())
      .def(py::init<const std::vector<std::vector<double>> &, int>())
      .def(py::init<const std::vector<std::vector<double>> &, int, int>())
      .def("transform", &KernelThermometer::transform);
  py::class_<KernelCanvasWrapper>(m, "KernelCanvas", py::module_local())
    .def(py::init<int, int, py::kwargs>())
    .def("transform", &KernelCanvasWrapper::transform);

  // means functions
  py::class_<Mean>(m, "Mean", py::module_local());
  py::class_<PowerMean, Mean>(m, "PowerMean", py::module_local()).def(py::init<int>());
  py::class_<Median, Mean>(m, "Median", py::module_local()).def(py::init());
  py::class_<HarmonicMean, Mean>(m, "HarmonicMean", py::module_local()).def(py::init());
  py::class_<HarmonicPowerMean, Mean>(m, "HarmonicPowerMean", py::module_local()).def(py::init<int>());
  py::class_<GeometricMean, Mean>(m, "GeometricMean", py::module_local()).def(py::init());
  py::class_<ExponentialMean, Mean>(m, "ExponentialMean", py::module_local()).def(py::init());

  // models
  py::class_<RegressionWisardWrapper>(m, "RegressionWisard", py::module_local())
      .def(py::init<int, py::kwargs>())
      .def("train", (void (RegressionWisardWrapper::*)(const std::vector<std::vector<int>> &, const std::vector<double> &)) & RegressionWisardWrapper::train)
      .def("train", (void (RegressionWisardWrapper::*)(const RegressionDataSet &)) & RegressionWisardWrapper::train)
      .def("predict", (std::vector<double>(RegressionWisardWrapper::*)(const std::vector<std::vector<int>> &)) & RegressionWisardWrapper::predict)
      .def("predict", (std::vector<double>(RegressionWisardWrapper::*)(const RegressionDataSet &)) & RegressionWisardWrapper::predict);

  py::class_<ClusRegressionWisardWrapper>(m, "ClusRegressionWisard", py::module_local())
      .def(py::init<int, double, int, int, py::kwargs>())
      .def("train", (void (ClusRegressionWisardWrapper::*)(const std::vector<std::vector<int>> &, const std::vector<double> &)) & ClusRegressionWisardWrapper::train)
      .def("train", (void (ClusRegressionWisardWrapper::*)(const RegressionDataSet &)) & ClusRegressionWisardWrapper::train)
      .def("predict", (std::vector<double>(ClusRegressionWisardWrapper::*)(const std::vector<std::vector<int>> &)) & ClusRegressionWisardWrapper::predict)
      .def("predict", (std::vector<double>(ClusRegressionWisardWrapper::*)(const RegressionDataSet &)) & ClusRegressionWisardWrapper::predict);
}
