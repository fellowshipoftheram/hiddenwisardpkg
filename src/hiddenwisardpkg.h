//base
#include "base.h"

//__version__
#include "version.h"

//libs - must be the first of all
#include "libs/json.hpp"
namespace nl = nlohmann;

//common
#include "common/definetypes.cc"
#include "common/exceptions.cc"
#include "common/utils.cc"

//data
#include "data/regressionbininput.cc"
#include "data/regressiondataset.cc"

//binarization
#include "binarization/base.cc"
#include "binarization/kernelcanvas.cc"
#include "binarization/kernelthermometer.cc"

//regression wisard model
#include "models/regressionwisard/meansfunctions.cc"
#include "models/regressionwisard/regressionram.cc"
#include "models/regressionwisard/regressionwisard.cc"

//clus regression wisard model
#include "models/clusregressionwisard/clusregressionwisard.cc"

//wrappers
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

// #include "wrappers/datawrapper.cc"
#include "wrappers/kernelcanvaswrapper.cc"
#include "wrappers/regressionwisardwrapper.cc"
#include "wrappers/clusregressionwisardwrapper.cc"
