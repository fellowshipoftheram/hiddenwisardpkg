class KernelThermometer{
public:
  KernelThermometer(const std::vector<std::vector<double>>& sequenceData, int numberOfKernels, int bitsByKernel=3):
    bitsByKernel(bitsByKernel){

    checkData(sequenceData);
    dim = sequenceData[0].size();
    checkInputs(numberOfKernels, dim, bitsByKernel);
    std::srand(time(NULL));

    means.resize(dim);
    multDimMean(sequenceData);
    stds.resize(dim);
    multDimStd(sequenceData);

    kernels.resize(numberOfKernels);
    for(int i=0; i<numberOfKernels; i++){
      kernels[i].resize(dim);
      for(int d=0; d<dim; d++){
        kernels[i][d] = randdouble(-1.0,1.0);
      }
    }
  }

  void multDimMean(const std::vector<std::vector<double>>& sequenceData){
    for(unsigned int i=0; i<means.size(); i++) means[i]=0;
    for(unsigned int i=0; i<sequenceData.size(); i++){
      for(int j=0; j<dim; j++){
        means[j] += sequenceData[i][j];
      }
    }
    for(unsigned int i=0; i<means.size(); i++) means[i] /=sequenceData.size();
  }

  void multDimStd(const std::vector<std::vector<double>>& sequenceData){
    for(unsigned int i=0; i<stds.size(); i++) stds[i]=0;
    for(unsigned int i=0; i<sequenceData.size(); i++){
      for(int j=0; j<dim; j++){
        double diff = (means[j] - sequenceData[i][j]);
        stds[j] +=  diff*diff;
      }
    }
    for(unsigned int i=0; i<stds.size(); i++) stds[i] = sqrt(stds[i]/sequenceData.size());
  }

  std::vector<int> activateKernels(const std::vector<double>& point){
    std::vector<double> distances(kernels.size());
    std::vector<int> output(kernels.size()*bitsByKernel);
    for(unsigned int j=0; j<output.size(); j++) output[j]=0;

    double maxDistance = 0;
    for(unsigned int i=0; i<kernels.size(); i++){
      double distance = 0;
      for(int j=0; j<dim; j++){
        double diff = (point[j] - kernels[i][j]);
        distance += diff*diff;
      }
      distance = sqrt(distance);
      distances[i] = distance;
      if(distance > maxDistance){
        maxDistance = distance;
      }
    }

    for(unsigned int i=0; i<distances.size(); i++){
      if(maxDistance>0){
        double d = 1.0 - distances[i]/maxDistance;
        int bits = (int)round(bitsByKernel*d);
        for(int k=0; k<bits; k++) output[i*bitsByKernel + k] = 1;
      }
    }
    return output;
  }

  std::vector<std::vector<int>> transform(const std::vector<std::vector<double>>& sequenceData){
    std::vector<std::vector<int>> output(sequenceData.size());
    std::vector<double> point(dim);

    for(unsigned int i=0; i<sequenceData.size(); i++){
      checkDimension(sequenceData[i].size());
      for(int j=0; j<dim; j++){
        point[j] = tanh((means[j] - sequenceData[i][j])/stds[j]);
      }
      output[i] = activateKernels(point);
    }
    return output;
  }

  ~KernelThermometer(){
    kernels.clear();
  }

private:
  int bitsByKernel;
  int dim;
  std::vector<std::vector<double>> kernels;
  std::vector<double> means;
  std::vector<double> stds;

  void checkData(const std::vector<std::vector<double>>& data){
    if(data.size() == 0)
      throw Exception("Error: there is no data passed to the constructor!");
  }

  void checkInputs(int numberOfKernels, int dim, int bitsByKernel){
    if(numberOfKernels < 1)
      throw Exception("Error: the number of kernels can not be lesser than 1!");
    if(dim < 1)
      throw Exception("Error: the dimension can not be lesser than 1!");
    if(bitsByKernel<1)
      throw Exception("Error: the number of bits by kernel can not be lesser than 1!");
  }

  void checkDimension(int size){
    if(size != dim)
      throw Exception("Error: the dimension of input data is defferent of kernel's dimension!");
  }
};
