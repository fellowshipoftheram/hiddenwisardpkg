class BinBase {
  template<typename T>
  RegressionBinInput transform(const std::vector<T>& data);
};
