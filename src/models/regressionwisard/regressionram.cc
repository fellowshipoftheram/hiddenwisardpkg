class RegressionRAM{
public:
  RegressionRAM(){}

  RegressionRAM(const std::vector<int> indexes, const int minZero = 0, const int minOne = 0) : addresses(indexes), minZero(minZero), minOne(minOne){}

  ~RegressionRAM(){
    addresses.clear();
    positions.clear();
  }

  template <typename T>
  void train(const T &image, const double y){
    addr_t index = getIndex<T>(image);
    if (NO_ADDRESS){
      return;
    }

    auto it = positions.find(index);
    if (it == positions.end())
      positions.insert(it, std::pair<addr_t, content_regression_t>(index, {1, y, 0}));
    else{
      it->second[0]++;
      it->second[1] += y;
    }
  }

  template <typename T>
  content_regression_t getVote(const T &image){
    addr_t index = getIndex<T>(image);
    if (NO_ADDRESS){
      return {0, 0};
    }

    auto it = positions.find(index);
    if (it == positions.end())
      return {0, 0};
    else
      return {it->second[0], it->second[1]};
  }

  template <typename T>
  void calculateFit(const T &image, const double yFit)
  {
    addr_t index = getIndex<T>(image);
    if (NO_ADDRESS){
      return;
    }

    auto it = positions.find(index);
    it->second[2] += yFit;
  }

  void applyFit(){
    for (auto it = positions.begin(); it != positions.end(); ++it){
      it->second[1] += it->second[2] / it->second[0];
      it->second[2] = 0;
    }
  }

protected:
  template <typename T>
  addr_t getIndex(const T &image){
    NO_ADDRESS = false;
    addr_t index = 0;
    addr_t p = 1;
    int countOne = 0;
    for (size_t i = 0; i < addresses.size(); i++){
      int bin = image[addresses[i]];
      countOne += bin;
      index += bin * p;
      p *= 2;
    }

    if ((countOne < minOne) || (((int)addresses.size() - countOne) < minZero)){
      NO_ADDRESS = true;
    }

    return index;
  }

private:
  std::vector<int> addresses;
  ram_regression_t positions;
  int minZero;
  int minOne;
  bool NO_ADDRESS;
};
