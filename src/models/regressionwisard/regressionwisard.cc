class RegressionWisard {
public:
  RegressionWisard(int addressSize) : addressSize(addressSize) {}
  RegressionWisard(int addressSize, bool completeAddressing, bool orderedMapping,
                  Mean* mean, int minZero, int minOne, int steps=0):
                  addressSize(addressSize), completeAddressing(completeAddressing),
                  orderedMapping(orderedMapping), mean(mean),
                  minZero(minZero), minOne(minOne), steps(steps), numberOfTrainings(0){

    srand(randint(0, 100000));

    checkMinZeroOne(minZero, minOne);
  }

  ~RegressionWisard(){
    rams.clear();
  }

  std::vector<int> getVotes(const std::vector<int> &image){
    return getVotes<std::vector<int>>(image);
  }

  std::vector<int> getVotes(const RegressionBinInput &image){
    return getVotes<RegressionBinInput>(image);
  }

  void train(const RegressionDataSet& dataset) {
    for (size_t i = 0; i < dataset.size(); i++)
    {
      train<RegressionBinInput>(dataset.getInput(i), dataset.getY(i));
    }

    for (int j = 0; j < steps; j++)
    {
      for (size_t i = 0; i < dataset.size(); i++)
      {
        calculateFit(dataset.getInput(i), dataset.getY(i));
      }
      applyFit();
    }
  }

  void train(const std::vector<int> &image, const double y){
    train<std::vector<int>>(image, y);
  }

  void train(const RegressionBinInput &image, const double y){
    train<RegressionBinInput>(image, y);
  }

  void train(const std::vector<std::vector<int>>& images, const std::vector<double>& Y){
    for (size_t i = 0; i < images.size(); i++){
      train(images[i],Y[i]);
    }

    for (int j = 0; j < steps; j++){
      for (size_t i = 0; i < images.size(); i++){
        calculateFit(images[i],Y[i]);
      }
      applyFit();
    }
  }

  double predict(const std::vector<int> &image){
    return predict<std::vector<int>>(image);
  }

  double predict(const RegressionBinInput &image){
    return predict<RegressionBinInput>(image);
  }

  std::vector<double> predict(const std::vector<std::vector<int>>& images){
    std::vector<double> output(images.size());
    for (size_t i = 0; i < images.size(); i++){
      output[i] = predict(images[i]);
    }
    return output;
  }

  std::vector<double> predict(const RegressionDataSet &dataset)
  {
    std::vector<double> output(dataset.size());

    for (size_t i = 0; i < dataset.size(); i++)
    {
      output[i] = predict<RegressionBinInput>(dataset[i]);
    }

    return output;
  }

  void calculateFit(const std::vector<int> &image, const double y)
  {
    calculateFit<std::vector<int>>(image, y);
  }

  void calculateFit(const RegressionBinInput &image, const double y)
  {
    calculateFit<RegressionBinInput>(image, y);
  }

  void applyFit(){
    for (size_t i = 0; i < rams.size(); i++){
      rams[i].applyFit();
    }
  }

  int getNumberOfTrainings(){
    return numberOfTrainings;
  }

protected:
  void setRAMShuffle(int entrySize){
    this->entrySize = entrySize;
    checkAddressSize(entrySize, addressSize);
    int numberOfRAMS = entrySize / addressSize;
    int remain = entrySize % addressSize;
    int indexesSize = entrySize;
    if(completeAddressing && remain > 0) {
      numberOfRAMS++;
      indexesSize += addressSize-remain;
    }

    rams.resize(numberOfRAMS);
    std::vector<int> indexes(indexesSize);

    for (int i = 0; i < entrySize; i++){
      indexes[i]=i;
    }
    for (size_t i=entrySize; i<indexes.size(); i++){
      indexes[i] = randint(0, entrySize-1, false);
    }

    if(!orderedMapping)
      random_shuffle(indexes.begin(), indexes.end());

    for (size_t i=0; i<rams.size(); i++){
      std::vector<int> subIndexes(indexes.begin() + (i*addressSize), indexes.begin() + ((i+1)*addressSize));
      rams[i] = RegressionRAM(subIndexes, minZero, minOne);
    }
  }

  template <typename T>
  std::vector<int> getVotes(const T &image)
  {
    checkEntrySize(image.size());
    std::vector<int> output(rams.size());
    for (size_t i = 0; i < rams.size(); i++){
      auto w = rams[i].getVote(image);
      output[i] = w[0];
    }
    return output;
  }

  template <typename T>
  void train(const T &image, const double y)
  {
    if (rams.empty())
      setRAMShuffle(image.size());
    checkEntrySize(image.size());
    numberOfTrainings++;
    for (size_t i = 0; i < rams.size(); i++)
    {
      rams[i].train(image, y);
    }
  }

  template <typename T>
  double predict(const T &image){
    checkEntrySize(image.size());
    std::vector<std::vector<double>> outputRams(rams.size());
    for (size_t i = 0; i < rams.size(); i++){
      outputRams[i] = rams[i].getVote(image);
    }
    return mean->calculate(outputRams);
  }

  template <typename T>
  void calculateFit(const T &image, double y){
    double yPredicted = predict(image);
    for (size_t i = 0; i < rams.size(); i++){
      rams[i].calculateFit(image, (y - yPredicted));
    }
  }

  void checkEntrySize(const int entry) const {
    if(entrySize != entry){
      throw Exception("The entry size defined on creation of RAM is different of entry size given as input!");
    }
  }

  void checkAddressSize(const int entrySize, const int addressSize) const{
    if( addressSize < 2){
      throw Exception("The address size cann't be lesser than 2!");
    }
    if( entrySize < 2 ){
      throw Exception("The entry size cann't be lesser than 2!");
    }
    if( entrySize < addressSize){
      throw Exception("The address size cann't be bigger than entry size!");
    }
  }

  void checkMinZeroOne(int min0, int min1){
    if(min0+min1 > addressSize){
      throw Exception("minZero + minOne is bigger than addressSize!");
    }
  }
  
    int addressSize;
    bool completeAddressing;
    bool orderedMapping;
    Mean* mean;
    int minZero;
    int minOne;
    int steps;

    int numberOfTrainings;
    int entrySize;
    std::vector<RegressionRAM> rams;

};
