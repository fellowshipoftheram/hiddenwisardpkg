

class ClusRegressionWisard {
public:
  ClusRegressionWisard(){}

  ClusRegressionWisard(int addressSize, double minScore, int threshold, int limit) : addressSize(addressSize), minScore(minScore), threshold(threshold), limit(limit) {}

  double getScore(const std::vector<int>& votes) const{
    int max = 0;
    double sum = 0;
    for(auto v: votes){
      if(v>max) max=v;
      sum += v;
    }

    if(max==0) return 0;

    return sum/(max*votes.size());
  }


  void train(const RegressionDataSet& dataset) {
    for(size_t i=0; i<dataset.size(); i++){
      train<RegressionBinInput>(dataset.getInput(i), dataset.getY(i));
    }
  }

  void train(const std::vector<std::vector<int>>& images, const std::vector<double>& Y){
    for (unsigned int i = 0; i < images.size(); i++) {
      train<std::vector<int>>(images[i], Y[i]);
    }
  }

  void train(const std::vector<int> &image, const double y){
    train<std::vector<int>>(image, y);
  }

  void train(const RegressionBinInput &image, const double y){
    train<RegressionBinInput>(image, y);
  }

  double predict(const std::vector<int> &image)
  {
    return predict<std::vector<int>>(image);
  }

  double predict(const RegressionBinInput &image)
  {
    return predict<RegressionBinInput>(image);
  }

  std::vector<double> predict(const std::vector<std::vector<int>> &images)
  {
    std::vector<double> output(images.size());

    for(size_t i = 0; i < images.size(); i++)
    {
      output[i] = predict<std::vector<int>>(images[i]);
    }

    return output;
  }

  std::vector<double> predict(const RegressionDataSet & dataset)
  {
    std::vector<double> output(dataset.size());

    for (size_t i = 0; i < dataset.size(); i++)
    {
      output[i] = predict<RegressionBinInput>(dataset[i]);
    }

    return output;
  }

  std::vector<double> predict(const std::vector<RegressionBinInput> &images)
  {
    std::vector<double> output(images.size());

    for (size_t i = 0; i < images.size(); i++)
    {
      output[i] = predict<RegressionBinInput>(images[i]);
    }

    return output;
  }

  unsigned int getNumberOfRegressionWisards(){
    return rews.size();
  }

  ~ClusRegressionWisard(){
    for(unsigned int i=0; i<rews.size(); ++i){
      delete rews[i];
    }
  }

protected:
  template <typename T>
  void train(const T& image, const double y){
    if(rews.size() == 0){
      makeRegressionWisard(0);
      rews[0]->train(image, y);
      return;
    }

    double bestValue = 0.0;
    bool trained = false;
    RegressionWisard* bestOne = NULL;

    for (unsigned int i = 0; i < rews.size(); i++)
    {
      auto votes = rews[i]->getVotes(image);
      double score = getScore(votes);
      double count = rews[i]->getNumberOfTrainings();

      if (score >= bestValue)
      {
        bestValue = score;
        bestOne = rews[i];
      }

      double limit = minScore + count / threshold;
      limit = limit > 1.0 ? 1.0 : limit;

      if (score >= limit)
      {
        rews[i]->train(image, y);
        trained = true;
      }
    }

    if(!trained && (int)rews.size() < limit){
      int index = rews.size();
      makeRegressionWisard(index);
      rews[index]->train(image, y);
      trained = true;
    }

    if(!trained && bestOne != NULL){
      bestOne->train(image, y);
    }
  }

  template <typename T>
  double predict(const T &image)
  {
    double bestScore = 0.0;
    double bestValue = 0.0;
    double localScore = 0.0;

    for (size_t i = 0; i < rews.size(); i++)
    {
      localScore = getScore(rews[i]->getVotes(image));

      if (localScore > bestScore)
      {
        bestValue = rews[i]->predict(image);
        bestScore = localScore;
      }
    }

    return bestValue;
  }

  std::map<int,RegressionWisard*> rews;
  unsigned int addressSize;
  double minScore;
  unsigned int threshold;
  int limit;
  bool completeAddressing;
  bool orderedMapping;
  Mean* mean;
  int minZero;
  int minOne;

  void makeRegressionWisard(const int index){
    rews[index] = new RegressionWisard(addressSize, completeAddressing, orderedMapping, mean, minZero, minOne);
  }
};
