class ClusRegressionWisardWrapper : public ClusRegressionWisard
{
  public:
    ClusRegressionWisardWrapper(int addressSize, double minScore, int threshold, int limit, py::kwargs kwargs) : ClusRegressionWisard(addressSize, minScore, threshold, limit)
    {
        completeAddressing = true;
        orderedMapping = false;
        mean = new SimpleMean();
        minZero = 0;
        minOne = 0;

        srand(randint(0, 100000));

        for (auto arg : kwargs)
        {
            if (std::string(py::str(arg.first)).compare("orderedMapping") == 0)
                orderedMapping = arg.second.cast<bool>();

            if (std::string(py::str(arg.first)).compare("minZero") == 0)
                minZero = arg.second.cast<int>();

            if (std::string(py::str(arg.first)).compare("minOne") == 0)
                minOne = arg.second.cast<int>();

            if (std::string(py::str(arg.first)).compare("completeAddressing") == 0)
                completeAddressing = arg.second.cast<bool>();

            if (std::string(py::str(arg.first)).compare("mean") == 0)
            {
                mean = arg.second.cast<Mean*>();
                mean = mean->clone();
            }
        }
    }
};
