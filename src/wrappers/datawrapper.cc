template <typename T>
class DataWrapper{
public:
  DataWrapper(py::array_t<T>& sequenceData){
    data = sequenceData.request();
    if(data.ndim != 2){
      throw Exception("The input dimension must be 2!");
    }
  }

  T operator ()(unsigned int i, unsigned int j){
    T* ptrData = (T*)data.ptr;
    unsigned int line = data.shape[1];
    return ptrData[i*line + j];
  }

private:
  py::buffer_info data;
};