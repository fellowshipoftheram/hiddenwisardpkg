typedef unsigned long long addr_t;
typedef unsigned long long index_size_t;
typedef char bin_t;

// Regression
typedef std::vector<double> content_regression_t;
typedef std::unordered_map<addr_t, content_regression_t> ram_regression_t;
const std::string regression_dataset_sufix = ".hwpkds";
