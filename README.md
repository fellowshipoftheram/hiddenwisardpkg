# hiddenwisardpkg

## Description:
This is a project to make available the different models based on WiSARD,
with high performance, easy to use and to install and following a pattern of use.
These provided models are machine learning models,
with supervised, unsupervised and semi-supervised learning.

## to install:
```
pip install git+https://bitbucket.org/fellowshipoftheram/hiddenwisardpkg.git
```
Works to python2 and pyhton3.  
If you are on Linux and not in a virtual environment, you may need to run as superuser.


## to uninstall:
```
pip uninstall hiddenwisardpkg
```

## to import:
```
import hiddenwisardpkg as hw
```

## to use:
### RegressionWiSARD

```python
  X = [
      [1,1,1,0,0,0,0,0,0],
      [1,1,1,1,0,0,0,0,0],
      [0,0,0,0,1,1,1,1,1],
      [0,0,0,0,0,1,1,1,1]
  ]

  y = [
      0.2,
      0.3,
      0.7,
      0.8
  ]
  for i,d in enumerate(X):
      print(y[i],d)


  print("\n")
  from hiddenwisardpkg import PowerMean
  addressSize = 2 # address size of the rams
  mean=PowerMean(3)
  wsd = RegressionWisard(addressSize, minZero=0, minOne=0, mean=mean)

  print("training...")
  wsd.train(X,y)

  print("predicting...")
  out=wsd.predict(X)

  print("out:")
  for i,d in enumerate(X):
      print(out[i],d)
```

## Documentation
You can find more detail about this library [here](https://bitbucket.org/fellowshipoftheram/hiddenwisardpkg/wiki/Home)


## Build on libraries:
[pybind11](https://github.com/pybind/pybind11)
[nlohmann/json](https://github.com/nlohmann/json)
