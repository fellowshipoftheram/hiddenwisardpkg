import hiddenwisardpkg as hw
import regression_dataset_test

print("\n\n")
print("### ReW ###")
print("### Input ###")

X = [
    [1,1,1,0,0,0,0,0,0],
    [1,1,1,1,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,1],
    [0,0,0,0,0,1,1,1,1]
]

y = [
    0.2,
    0.3,
    0.7,
    0.8
]
for i,d in enumerate(X):
    print(y[i],d)


print("\n")

addressSize = 3 # tamanho do endereçamento das rams
rew = hw.RegressionWisard(addressSize, minZero=0, minOne=0, mean=hw.ExponentialMean(), steps=20)

print("training...")
rew.train(X,y)

print("predicting...")
out=rew.predict(X)

print("out wsd:")
for i,d in enumerate(X):
    print(out[i],d)

print("### With RegressionDataSet ###")

rew2 = hw.RegressionWisard(addressSize, minZero=0, minOne=0, mean=hw.ExponentialMean(), steps=20)

print("training...")
rew.train(regression_dataset_test.ds)

print("predicting...")
out=rew.predict(regression_dataset_test.ds)

print("out wsd2:")
for i,d in enumerate(X):
    print(out[i], regression_dataset_test.ds.getY(i))

print("### DONE ReW ###")
