import hiddenwisardpkg as hw


print("\n\n")
print("### CReW ###")
print("### Input ###")

X = [
    [1,1,1,0,0,0,0,0,0],
    [1,1,1,1,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,1],
    [0,0,0,0,0,1,1,1,1]
]

y = [
    0.2,
    0.3,
    0.7,
    0.8
]
for i,d in enumerate(X):
    print(y[i],d)


print("\n")

addressSize = 3 
minScore = 0.1
threshold = 10
rewsLimit = 5
crew = hw.ClusRegressionWisard(addressSize, minScore, threshold, rewsLimit, minZero=0, minOne=0, mean=hw.ExponentialMean())

print("training...")
crew.train(X,y)

print("predicting...")
out=crew.predict(X)

print("out:")
for i,d in enumerate(X):
    print(out[i],d)

print("### DONE CReW ###")
