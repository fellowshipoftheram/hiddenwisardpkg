from hiddenwisardpkg import KernelThermometer
from random import random

print("\n\n")
print("### Kernel Thermometer  ###")

data = []
p = [10*random(),10*random()]
for i in range(5):
    point = list(p)
    point[0] += i
    point[1] += i
    data.append(point)

numberOfKernels = 10
bitsByKernel = 4
kt = KernelThermometer(data, numberOfKernels, bitsByKernel)

out = kt.transform(data)
print("binary output:",out)
print("### DONE Kernel Thermometer  ###")
