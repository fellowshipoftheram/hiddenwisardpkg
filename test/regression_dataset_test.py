import hiddenwisardpkg as hw


print("\n\n")
print("### RegressionDataSet ###")
print("### Input ###")

X = [
    [1,1,1,0,0,0,0,0,0],
    [1,1,1,1,0,0,0,0,0],
    [0,0,0,0,1,1,1,1,1],
    [0,0,0,0,0,1,1,1,1]
]

y = [
    0.2,
    0.3,
    0.7,
    0.8
]

for i,d in enumerate(X):
    print(y[i],d)

ds = hw.RegressionDataSet(X, y)

print(ds.size())

for i in range(ds.size()):
    print(i, ds.getY(i) == y[i], ds.getInput(i).data() == hw.RegressionBinInput(X[i]).data())

ds.save("test/testFile")

ds2 = hw.RegressionDataSet("test/testFile.hwpkds")

for i in range(ds.size()):
    print(i, ds2.getY(i) == y[i], ds2.getInput(i).data() == hw.RegressionBinInput(X[i]).data())

print("### DONE RegressionDataSet ###")
